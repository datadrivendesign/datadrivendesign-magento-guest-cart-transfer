<?php

namespace DataDrivenDesign\GuestCartTransfer\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Checkout\Model\Session;
use Magento\Quote\Model\QuoteFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_quoteManagement;
    protected $_quoteIdMaskFactory;
    protected $_cartRepository;
    protected $_session;
    protected $_quoteFactory;

    public function __construct(
        Context $context,
        CartManagementInterface $quoteManagement,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        CartRepositoryInterface $cartRepository,
        Session $session,
        QuoteFactory $quoteFactory
    )
    {
        $this->_quoteManagement = $quoteManagement;
        $this->_quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->_cartRepository = $cartRepository;
        $this->_session = $session;
        $this->_quoteFactory = $quoteFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $cartId = $this->getRequest()->getParam('id');

        // If no cart id is sent, redirect to cart
        if (!$cartId) {
            return $this->_redirect('checkout/cart/');
        }

        $quoteIdMask = $this->_quoteIdMaskFactory->create()->load($cartId, 'masked_id');

        // If quote id mask isn't found, redirect to cart
        if (!$quoteIdMask) {
            return $this->_redirect('checkout/cart/');
        }

        $quote = $this->_cartRepository->get($quoteIdMask->getQuoteId());

        // If no quote found matching the id mask, redirect to cart
        if (!$quote) {
            return $this->_redirect('checkout/cart/');
        }

        $this->_session->replaceQuote($quote);
        $this->_redirect('checkout/cart/');
    }
}